import { Component } from '@angular/core';
import { QueryparamsService } from './queryparams.service';
import { ProductService } from './product.service';
import { forkJoin } from 'rxjs';

import * as _ from 'lodash';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent {
  	title = 'productListModule';

	public categories: any;
	public tags: any;
	public colors: any;
	public sizes: any;
	public weights: any;
	public orderOpt: any = ["asc", "desc"];
	public orderbyOpt: any = ['title','slug','date'];
	public categorySlug:any = [];
	public tagSlug:any = [];
	public termSlug:any = [];
	
	private categoryId:any = [];
	private tagId:any = [];
	private termId:any = [];
	private queryParams:any = {};
	private TagsName = "brands";

	// ======================== //
	// product list parameter
	// ======================== //
	public pagination = {
		per_page: 3,
		page: 1
	};
	public filter:any = {
		search: null,
		category: null,
		min_price: null,
		max_price: null,
		tag:null,
	};
	public sorting:any = {
		order:null,
		orderby:null
	};
	public attribute:any = {};
	public productType = "list";


	constructor(
		private queryParamsService: QueryparamsService,
		private productService: ProductService
	){}

	ngOnInit(){
		this.getData();
	}

	// ============================= //
	// Getting data from server
	// ============================= //
	getData(){
		let categories = this.productService.getCategories();
		let tags = this.productService.getTags();
		let colors = this.productService.getAttributeTerm(2);
		let sizes = this.productService.getAttributeTerm(1);
		let weight = this.productService.getAttributeTerm(3);
		forkJoin([categories,tags,colors,sizes,weight]).subscribe((res)=>{
			this.categories = res[0];
			this.tags = res[1];
			this.colors = res[2];
			this.sizes = res[3];
			this.weights = res[4];
			this.getQueryParams();
		})
	}

	// ================================ //
	// filter value query params     
	// ================================ //
	filterQueryParams(collection,slugColect){
		let arrSlug: any = [];
		let arrId: any = [];
		if(slugColect){
			collection.map((e)=>{
				slugColect.map((o)=>{
					arrSlug.push(o);
					if(o==e.slug) {
						arrId.push(e.id);
					}else{
						arrId.push(0);
					};
				});
			});
		}else{
			arrSlug.push(slugColect);
			arrId.push(0);
		}
		return [ _.uniq(arrId) , _.uniq(arrSlug) ]
	}

	// ============================= //
	// Getting data from url
	// ============================= //
	getQueryParams(){
		let filter:any = {};
		let sorting:any = {};
		let pagination:any = {};
		let attribute:any = {};
		let params:any = this.queryParamsService.getQueryParams();
		_.map(params,(val,key)=>{
			switch (key) {
				case "category":
					let categorySlug = val.split(',');
					let filteredCategory = this.filterQueryParams(this.categories,categorySlug);
					[ this.categoryId , this.categorySlug ] = filteredCategory;
					this.queryParams[key] = _.join(this.categorySlug);
					filter[key] = _.join(this.categoryId);
					break;
			
				case this.TagsName:
					let tagSlug = val.split(',');
					let filteredTags = this.filterQueryParams(this.tags,tagSlug);
					[ this.tagId , this.tagSlug ] = filteredTags;
					this.queryParams[key] = _.join(this.tagSlug);
					filter['tag'] = _.join(this.tagId)
					break;

				case "min-price":
					this.queryParams[key] = val;
					filter['min_price'] = val;
					break;

				case "max-price":
					this.queryParams[key] = val;
					filter['max_price'] = val;
					break;
				
				case "search":
					this.queryParams[key] = val;
					filter['search'] = val;
					break

				case "pa_color":
					let colorSlug = val.split(',');
					let filteredColors = this.filterQueryParams(this.colors,colorSlug);
					[ this.termId, this.termSlug ] = filteredColors;
					this.queryParams[key] = _.join(this.termSlug);
					attribute =  _.assign(attribute,{attribute: key , attribute_term: _.join(this.termId)})
					break;

				case "pa_size":
					let sizeSlug = val.split(',');
					let filteredSizes = this.filterQueryParams(this.sizes,sizeSlug);
					[ this.termId, this.termSlug ] = filteredSizes;
					this.queryParams[key] = _.join(this.termSlug);
					attribute =  _.assign(attribute,{attribute: key, attribute_term: _.join(this.termId)})
					break;

				case "pa_weight":
					let weightSlug = val.split(',');
					let filteredWeight = this.filterQueryParams(this.weights,weightSlug);
					[ this.termId, this.termSlug ] = filteredWeight;
					this.queryParams[key] = _.join(this.termSlug);
					attribute =  _.assign(attribute,{attribute: key, attribute_term: _.join(this.termId)})
					break;

				case "order":
					if(_.includes(this.orderOpt,val)) sorting['order'] = val;
					this.queryParams['order'] = val;
					break;

				case "orderby":
					if(_.includes(this.orderbyOpt,val)) sorting['orderby'] = val;
					this.queryParams['orderby'] = val;
					break;
				
				case "page":
					let page = _.uniq(val.split(','));
					pagination['page'] = page[0];
					this.queryParams['page'] = page[0];
					break;
			}
		})
		this.filter = this.updateObject(this.filter,filter);
		this.sorting = this.updateObject(this.sorting,sorting);
		this.pagination = this.updateObject(this.pagination,pagination);
		this.attribute = this.updateObject(this.attribute,attribute);
		this.updateQueryParams();
	}

	// ============================= //
	// Setting data from url
	// ============================= //
	updateQueryParams(){
		this.queryParamsService.setQueryParams('',this.queryParams);
	}

	// ============================= //
	// Search Product
	// ============================= //
	searchProduct = _.debounce(function(key){
		if(key){
			this.queryParams = _.assign(this.queryParams,{ 'search' : key });
			this.filter = this.updateObject(this.filter,{ search: key });
			this.resetPage();
		}else{
			this.filter = this.updateObject(this.filter,{ search: null });
			delete this.queryParams['search'];
		}
		this.updateQueryParams();
	}, 1000);

	// ============================= //
	// filter by price
	// ============================= //
	filterByPrice = _.debounce(function(min,max){
		if(min){
			this.queryParams = _.assign(this.queryParams,{ 'min-price' : min });
			this.filter = this.updateObject(this.filter,{ min_price: min });
			this.resetPage();
		}else{
			delete this.queryParams['min-price']
			this.filter = this.updateObject(this.filter,{ min_price: null });
		}
		if(max){
			this.queryParams = _.assign(this.queryParams,{ 'max-price' : max });
			this.filter = this.updateObject(this.filter,{ max_price: max });
			this.resetPage();
		}else{
			delete this.queryParams['max-price']
			this.filter = this.updateObject(this.filter,{ max_price: null });
		}
		this.updateQueryParams();
	}, 1000);
	
	// ============================= //
	// filter by tags
	// ============================= //
	filterByTag(tag){
		if(_.includes(this.tagId,tag.id)){
			this.tagSlug = _.filter(this.tagSlug,(e)=> e != tag.slug);
			this.tagId = _.filter(this.tagId,(e)=> e != tag.id);
		}else{
			this.tagSlug.push(tag.slug);
			this.tagId.push(tag.id);
		}
		[this.tagSlug , this.tagId ] = this.clearSlugId(this.tags,this.tagSlug);
		if(!this.tagSlug.length){
			delete this.queryParams[this.TagsName];
			this.filter = this.updateObject(this.filter,{tag:null})
		}else{
			this.queryParams = _.assign(this.queryParams, { [this.TagsName]: _.join(this.tagSlug)});
			this.filter = this.updateObject(this.filter,{ tag : _.join(this.tagId)})
			this.resetPage();
		}
		this.updateQueryParams();
	}

	// ============================= //
	// filter by Attribute
	// ============================= //
	filterByAttribute(attr,term){
		if(this.attribute.attribute!=attr) {
			this.termId = [];
			this.termSlug = [];
			delete this.queryParams[this.attribute.attribute];
		}
		if(_.includes(this.termId,term.id)){
			this.termSlug = _.filter(this.termSlug,(e)=> e != term.slug);
			this.termId = _.filter(this.termId,(e)=> e != term.id);
		}else{
			this.termSlug.push(term.slug);
			this.termId.push(term.id);
		}
		switch (attr){
			case "pa_color":
				[this.termSlug , this.termId ] = this.clearSlugId(this.colors,this.termSlug);
				break;
				
			case "pa_size":
				[this.termSlug , this.termId ] = this.clearSlugId(this.sizes,this.termSlug);
				break;

			case "pa_weigth":
				[this.termSlug , this.termId ] = this.clearSlugId(this.weights,this.termSlug);
				break;
		}
		console.log(this.termSlug, this.termId);
		if(!this.termSlug.length){
			delete this.queryParams[attr];
			this.attribute = this.updateObject(this.attribute,{attribute_term: null, attribute: null });
		}else{
			this.queryParams = _.assign(this.queryParams,{[attr] : this.termSlug.join()});;
			this.attribute = this.updateObject(this.attribute,{attribute_term: this.termId.join(), attribute: attr });
			this.resetPage();
		}
		this.updateQueryParams();
	}

	// ============================= //
	// filter by category
	// ============================= //
	filterByCategory(category){
		if(_.includes(this.categoryId,category.id)){
			this.categorySlug = _.filter(this.categorySlug,(e)=> e != category.slug);
			this.categoryId = _.filter(this.categoryId,(e)=> e != category.id);
		}else{
			this.categorySlug.push(category.slug);
			this.categoryId.push(category.id);
		}
		[ this.categorySlug, this.categoryId ] = this.clearSlugId(this.categories,this.categorySlug);
		if(!this.categorySlug.length){
			delete this.queryParams['category'];
			this.filter = this.updateObject(this.filter,{category: null });
		}else{
			this.queryParams = _.assign(this.queryParams,{category : this.categorySlug.join()});;
			this.filter = this.updateObject(this.filter,{category: this.categoryId.join()});
			this.resetPage();
		}
		this.updateQueryParams();
		
	}

	// =====================================
	// Reset Page
	// =====================================
	resetPage(){
		this.pagination = this.updateObject(this.pagination, { page : 1 });
		delete this.queryParams['page'];
	}

	// =======================================
	// Clear array slug & array id
	// =======================================
	clearSlugId(colection,slug){
		let arrSlug:any = [];
		let arrId:any = [];
		_.map(colection,(e)=>{
			if(_.includes(slug,e.slug)){
				arrSlug.push(e.slug);
				arrId.push(e.id);
			}
		})
		return [arrSlug,arrId];
	}

	// ============================ //
	// change page
	// ============================//
	changePage(page){
		this.queryParams = _.assign(this.queryParams,{page:page});
		this.pagination = this.updateObject(this.pagination,{page: page});
		this.updateQueryParams();
	}

	// ============================ //
	// order 
	// ============================ //
	orderProduct(order){
		if(order){
			this.queryParams = _.assign(this.queryParams, {order:order} );
			this.sorting = this.updateObject(this.sorting,{order: order});
			this.resetPage()
		}else{
			delete this.queryParams['order'];
			this.sorting = this.updateObject(this.sorting,{order: null});
		}
		this.updateQueryParams();
	}

	// ============================= //
	// order by
	// ============================= //
	orderBy(order){
		if(order){
			this.queryParams = _.assign(this.queryParams,{orderby:order});
			this.sorting = this.updateObject(this.sorting,{orderby: order});
			this.resetPage()
		}else{
			delete this.queryParams['orderby'];
			this.sorting = this.updateObject(this.sorting,{orderby: null});
		}
		this.updateQueryParams();
	}

	// ============================= //
	// update an object
	// ============================= //
	updateObject(container,data){
		let containerClone = _.clone(container);
		return _.assign(containerClone,data)
	}
}
