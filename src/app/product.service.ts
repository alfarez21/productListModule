import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import * as _ from 'lodash';


@Injectable({
  	providedIn: 'root'
})
export class ProductService {
	apiUrl: string = "https://lundara.com/lnd-wp/wp-json/wc/v3/";
	apiKey: any = {
		consumer_key: "ck_2dbd5c2293eee74d398590413145988222f705fc",
		consumer_secret : "cs_ff40f5b3456d60e4d41aa2b4e268177d6d979e12"
  	};
  
  	constructor(
		private http: HttpClient, 
	) {}

  	// Get Products
	getProducts(): Observable<any[]>{
		return this.http.get<any[]>( this.apiUrl+"products", { params : this.apiKey });
	}

	// get filter product
	filter(data,param): Observable<any[]>{
		let params = _.assign(_.clone(param),this.apiKey);
		return this.http.get<any[]>( this.apiUrl+data, { params : params });
	}

	// Retrieve a product
	getProduct(id): Observable<any[]>{
		return this.http.get<any[]>( this.apiUrl+"products/"+id, { params : this.apiKey });
	}

	// get product categories
	getCategories(): Observable<any[]>{
		return this.http.get<any[]>( this.apiUrl+"products/categories", { params : this.apiKey });
	}

	// get product tags
	getTags(): Observable<any[]>{
		return this.http.get<any[]>( this.apiUrl+"products/tags", { params : this.apiKey });
	}

	// get product review
	getProductReviews(id): Observable<any[]>{
		return this.http.get<any[]>( this.apiUrl+"products/tags?product="+id, { params : this.apiKey });
	}

	// get attribute term
	getAttributeTerm(id): Observable<any[]>{
		return this.http.get<any[]>( this.apiUrl+"products/attributes/"+id+"/terms", { params : this.apiKey });
	}
}
