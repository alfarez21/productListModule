import { Component, Input, Output, EventEmitter, OnChanges } from '@angular/core';

import { ProductListService } from './product-list.service';

import * as _ from 'lodash';

@Component({
	selector: 'app-product-list',
	templateUrl: './product-list.component.html',
	styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnChanges {

	public products: any;
	public pages: any = [];
	
	@Input() public pagination: any;
	@Input() private filter: any;
	@Input() private attribute: any;
	@Input() private sorting: any;
	@Input() private productType: string;

	@Output() public pageOnChanges =  new EventEmitter;

	private apiParams: any;

	constructor(
		private productService: ProductListService
	) { }

	ngOnInit(){
	}

	ngOnChanges(): void {
		this.setApiParams();
	}
	
	// ============================ //
	//  Set Api Parameter
	// ============================ //
	private setApiParams(){
		this.apiParams = _.assign(this.apiParams,this.filter,this.pagination,this.sorting,this.attribute)
		_.map(this.apiParams,(val,key)=>{
			if(!this.apiParams[key]) delete this.apiParams[key];
		})
		this.filterProduct(this.apiParams)
		console.log(this.apiParams);
	}

	// ============================ //
	// filter products
	// ============================ //              
	private filterProduct(params): void {
		let params2 = _.clone(params);
		_.map(params2,(val,key)=>{
			if(key=="page"||key=="per_page") delete params2[key]
		})
		this.productService.filterProducts(params).subscribe((res)=>this.products = res)
		this.productService.filterProducts(params2).subscribe((res)=>{
			let pageLength = Math.ceil(res.length/this.pagination.per_page);
			this.pages = [];
			for(let i = 1; i <= pageLength ; i++){
				this.pages.push(i);
			}
		})
	} 
	
	// =========================== //
	// next page
	// =========================== //
	public next(){
		this.pagination.page++;
		this.pageOnChanges.emit(this.pagination.page);
	}

	// =========================== //
	// previous page
	// =========================== //
	public prev(){
		this.pagination.page--;
		this.pageOnChanges.emit(this.pagination.page);
	}

	// =========================== //
	// change to a specific page
	// =========================== //
	public changePage(page){
		this.pageOnChanges.emit(page);
	}
	  
}
