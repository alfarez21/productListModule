import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import * as _ from 'lodash';

@Injectable({
	providedIn: 'root'
})
export class ProductListService {

	private apiUrl: string = "https://lundara.com/lnd-wp/wp-json/wc/v3/";

	private apiKey: any = {
		consumer_key: "ck_2dbd5c2293eee74d398590413145988222f705fc",
	  	consumer_secret : "cs_ff40f5b3456d60e4d41aa2b4e268177d6d979e12"
	};

	constructor(
		private http: HttpClient
	) { }
	
	// ===================================== //
	//  filter products                      //
	// ===================================== //
	public filterProducts(params): Observable<any> {
		let param = _.assign(_.clone(params),this.apiKey);
		return this.http.get<any[]>( this.apiUrl+'products', { params : param });
	}
  
}
