import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class QueryparamsService {

  	constructor(
		private route: ActivatedRoute,
		private router: Router
	) { }

	public setQueryParams(prefix,params): void{
		this.router.navigate([prefix], { queryParams: params });
	}

	public getQueryParams(){
		let params: any = this.route.snapshot.queryParams;
		return Object.assign({},params);
	}
}
